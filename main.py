from tkinter_component import *
import threading
import time
from threading import Timer
import random
import contextlib
from roadobject import *
from policy import *
from object_info import *
from pyDatalog.pyDatalog import assert_fact, ask, clear
from tkinter import messagebox

pyDatalog.create_atoms('tr_violation, ID, XPOS, YPOS, IN_LANE, VIOLATION')
# LANE = [LEFT, MIDDLE, RIGHT]
LANENAME = ['LEFT', 'MIDDLE', 'RIGHT']


def iniroadobjs(numlist, roadobjs, trees):
    # print(numlist)
    num = 0
    for j in range(2):
        for i in range(len(numlist[j])):
            if numlist[j][i] <= 4:
                ypos = LEFT
                xpos = numlist[j][i] * PICWIDTH
            elif numlist[j][i] <= 9:
                ypos = MIDDLE
                xpos = (numlist[j][i] - 5) * PICWIDTH
            else:
                ypos = RIGHT
                xpos = (numlist[j][i] - 10) * PICWIDTH
            if j == 0:
                roadobjs.append(RoadObject(int(xpos), int(ypos), "sidecar", num))
            else:
                roadobjs.append(RoadObject(int(xpos), int(ypos), "roadblock", num))
            num += 1
    # initialize trees
    """
    for i in range(2):
        for j in range(5):
            trees.append(Tree(j*120+30, (HEIGHT+GREENBELT)*i))
    """


def handle_out_range_objs(cars, roadblocks):
    """move side cars"""
    i = 0
    for obj in cars:
        newypos = LANE[i%3]
        obj.xspeed = random.choice(SIDECAR_SPEED)
        obj.inispeed = obj.xspeed
        # newxpos = -120*(i//3 + 2)
        newxpos = -120*(i//3)
        canvas.move(obj.shape, -obj.getxpos()+newxpos, -obj.getypos()+newypos)
        obj.state = "standby"
        i += 1
    # move roadblocks
    LANE_COPY = LANE[:]
    for obj in roadblocks:
        newypos = random.choice(LANE_COPY)
        LANE_COPY.remove(newypos)
        canvas.move(obj.shape, WIDTH+PICWIDTH, -obj.getypos()+newypos)
        obj.state = "standby"


def maincarmoving(roadobjs):
    n = 0
    while n < TURN_LOOP:
        for obj in roadobjs:
            canvas.move(obj.shape, obj.xspeed, obj.yspeed)
            obj.outofcanvas()
            window.update()
            time.sleep(0.01)
        n += 1
    """back to initial state"""
    for obj in roadobjs:
        obj.reverse()


def animate(roadobjs):
    carlist = []
    bklist = []
    maincarmoving(roadobjs)
    # carlist.clear()
    # bklist.clear()


def animatetree(trees, maincar):
    while True:
        if maincar.state != "stop":
            for tree in trees:
                tree.move()
        window.update()
        time.sleep(UPDATE_INTERVAL)


@contextlib.contextmanager
def get_session():
    s = Session()
    try:
        yield s
        s.commit()
    except Exception as e:
        s.rollback()
        raise e
    finally:
        s.close()


def checkcollision(obj1, obj2):
    dis = obj1.xPosition - obj2.xPosition
    if (0 <= dis < COLLIDELIMIT and obj1.xSpeed <= obj2.xSpeed) or \
        (0 <= -dis < COLLIDELIMIT and obj1.xSpeed >= 0):
        return True
    else:
        return False


def checkviolation(resultlist, obj, currentlane, var, name):
    assert_fact('tr_violation', var, obj.idx, 'Continue')
    # check violation for turn left
    if currentlane == 0:  # "LEFT" lane
        assert_fact('tr_violation', var, obj.idx, 'Turn left')
    else:  # obj in "MIDDLE" or "RIGHT" lane
        t = currentlane - 1
        for j in range(len(resultlist[t])):
            if checkcollision(resultlist[t][j], obj):
                assert_fact('tr_violation', var, obj.idx, 'Turn left')
                break
    # check violation for turn right
    if currentlane == 2:  # "RIGHT" lane
        assert_fact('tr_violation', var, obj.idx, 'Turn right')
    else:  # obj in "LEFT" or "MIDDLE" lane
        t = currentlane + 1
        for j in range(len(resultlist[t])):
            if checkcollision(resultlist[t][j], obj):
                assert_fact('tr_violation', var, obj.idx, 'Turn right')
    name.append(obj.idx)


def preprocess(resultlist, var):
    flag = False
    name = []
    for i in range(3):
        if len(resultlist[i]) > 1:
            for j in range(len(resultlist[i]) - 1):
                obj = resultlist[i][j + 1]
                dis = resultlist[i][j].xPosition - obj.xPosition
                if 0 < dis < COLLIDELIMIT and (obj.objectType != "roadblock" and obj.objectState != "stopped"):
                    # print('collide item', obj.idx)
                    checkviolation(resultlist, obj, i, var, name)
                    flag = True
    if not flag:
        print('var', var)
        assert_fact('tr_violation', var, None, None)
        assert_fact('suggested_action', var, None, None)
    return name


def processing(roadobjs):
    with get_session() as s:
        resultlist1 = []
        resultlist2 = []
        for i in range(3):
            result1 = s.query(ObjInfo).filter(ObjInfo.laneName == LANENAME[i]).order_by(desc(ObjInfo.xPosition)).all()
            resultlist1.append(result1)
            if not result1:
                + lane_free(LANENAME[i])
            else:
                assert_fact('lane_free', None)
            for item in result1:
                + object_in_lane('v1', item.idx, LANENAME[i])
                + position_of('v1', item.idx, item.xPosition, item.yPosition)

            """randomly delete some objects"""
            result2 = result1[:]
            num = random.randint(1, 6)
            # if result2 and num > 3:
            #     rnditem = random.choice(result2)
            #     result2.remove(rnditem)
            if result2:
                rnditem = random.choice(result2)
                result2.remove(rnditem)
                for item in result2:
                    + object_in_lane('v2', item.idx, LANENAME[i])
                    + position_of('v2', item.idx, item.xPosition, item.yPosition)
            resultlist2.append(result2)
        # print('free lane', ask('lane_free(XX)'))
        # print('object_in_lane:\n', object_in_lane(ID, IN_LANE))
        # print('position_of\n', ask('position_of(ID, XPOS, YPOS)'))
            # print('position_of:\n', position_of(ID, XPOS, YPOS), file=f1)
        "preprocess provides suggest actions"
        # print(resultlist1)
        # print(resultlist2)
        # return True
        lst1 = preprocess(resultlist1, "v1")
        lst2 = preprocess(resultlist2, "v2")
        if lst1:
            dct1 = varient1(lst1)
        else:
            dct1 = {}
        # print('main dct1', dct1)
        if lst2:
            dct2 = varient2(lst2)
        else:
            dct2 = {}
        # print('main dct2', dct2)
        """call supervisor to decide final actions"""
        ter_flag = supervisor(roadobjs, dct1, dct2)

        if ter_flag:
            return ter_flag
        """animate process take actions"""
        animate(roadobjs)
        """update ObjInfo"""
        for i in range(NUM_OF_OBJ):
            session.query(ObjInfo).filter(ObjInfo.idx == i).\
                update({'laneName':roadobjs[i].getlane(), 'xPosition': roadobjs[i].getxpos(), 'yPosition':roadobjs[i].getypos(),
                        'objectState':roadobjs[i].state, 'xSpeed':roadobjs[i].xspeed})
            session.commit()
        clear()
        return False


def start(roadobjs):
    """create tables"""
    Base.metadata.create_all(engine)
    with get_session() as s:
        for obj in roadobjs:
            objinfo = ObjInfo(obj.idx, obj.getlane(), obj.getxpos(), obj.getypos(), obj.type, obj.state, obj.xspeed)
            session.add(objinfo)
        session.commit()
    while True:
        ter_flag = processing(roadobjs)
        if ter_flag:
            global global_count
            global_count = 0
            global DICT_LAST
            DICT_LAST = {}
            Restartapplication()


def Restartapplication():
    MsgBox = tk.messagebox.askquestion('Restart Application', 'Do you want to restart', icon='warning')
    if MsgBox == 'yes':
        window.destroy()
    else:
        window.destroy()


def main():
    """initialize"""
    trees = []
    maincar = RoadObject(WIDTH/2 - 60, MIDDLE, "maincar", NUM_OF_OBJ - 1)
    roadobjs = []
    regionlist = [0, 1, 3, 4, 5, 8, 9, 10, 11, 13, 14]
    number1 = random.sample(regionlist, NUM_OF_SIDECARS)
    regionlist = [x for x in regionlist if x not in number1]
    number2 = random.sample(regionlist, NUM_OF_ROADBLOCKS)
    numlist = [number1, number2]
    iniroadobjs(numlist, roadobjs, trees)
    roadobjs.append(maincar)
    """assert facts"""
    start(roadobjs)
    canvas.mainloop()


if __name__ == '__main__':
    main()