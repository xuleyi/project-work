from sqlalchemy import create_engine, desc, func
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from pyDatalog import pyDatalog

# define a base class with SQLAlchemy and datalog capabilities, to be inherited by the Employee class
Base = declarative_base(cls=pyDatalog.Mixin, metaclass=pyDatalog.sqlMetaMixin)
# open a session on a database, then associate it to the Base class
engine = create_engine('sqlite:///:memory:', echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base.session = session


class ObjInfo(Base):  # --> Employee inherits from the Base class
    __tablename__ = 'objinfo'

    # objectName = Column(String(32))
    idx = Column(Integer, primary_key=True)
    laneName = Column(String(32))
    xPosition = Column(Integer)
    yPosition = Column(Integer)
    objectType = Column(String(32))
    objectState = Column(String(32))
    xSpeed = Column(Integer)

    def __init__(self, idx, lane, xpos, ypos, type, state, xspeed):
        super(ObjInfo, self).__init__()
        # self.objectName = name
        self.idx = idx
        self.laneName = lane
        self.xPosition = xpos
        self.yPosition = ypos
        self.objectType = type
        self.objectState = state
        self.xSpeed = xspeed

    def __repr__(self):
        # return "<ObjInfo(name='%s', lane='%s', xpos='%d', ypos='%d', type='%s', state='%s')>" % (
        #     self.objectName, self.laneName, self.xPosition, self.yPosition, self.objectType, self.objectState)
        return "<ObjInfo(idx='%d', type='%s', xpos='%d', ypos='%d')>" % (self.idx, self.objectType, self.xPosition, self.yPosition)