from pyDatalog import pyDatalog
from pyDatalog.pyDatalog import assert_fact, ask
import math
import time
TURN_STEP = 7
MAINCAR_SPEED = 2
COLLIDELIMIT = 180  # must > maincar speed*2*20
NUM_OF_SIDECARS = 3
NUM_OF_ROADBLOCKS = 3
NUM_OF_OBJ = NUM_OF_SIDECARS + NUM_OF_ROADBLOCKS + 1
action_priority = {0: 'Turn left', 1: 'Turn right', 2: 'Stop'}
DICT_LAST = {}

global_count = 0
# TERMINTATE = False
# create terms
pyDatalog.create_atoms('object_in_lane, position_of, lane_free, tr_violation, ACTION_v1, ACTION_v2, ID_v1, ID_v2, V1,\
                    VAR, ID, ACTION, obj, Can_turn_left, Can_turn_right, suggested_action, V2, VIOLATION, ID_TR, VAR, \
                    Does_varient_selfcontradict, Does_varients_contradict')


def loadrule():
    """define rules"""
    Can_turn_left(VAR, ID) <= object_in_lane(VAR, ID, "MIDDLE") & lane_free("LEFT")
    Can_turn_left(VAR, ID) <= object_in_lane(VAR, ID, "MIDDLE") & ~(lane_free("LEFT")) & ~tr_violation(VAR, ID, "Turn left")
    Can_turn_left(VAR, ID) <= object_in_lane(VAR, ID, "RIGHT") & lane_free("MIDDLE")
    Can_turn_left(VAR, ID) <= object_in_lane(VAR, ID, "RIGHT") & ~(lane_free("MIDDLE")) & ~tr_violation(VAR, ID, "Turn left")
    Can_turn_right(VAR, ID) <= object_in_lane(VAR, ID, "MIDDLE") & lane_free("RIGHT")
    Can_turn_right(VAR, ID) <= object_in_lane(VAR, ID, "MIDDLE") & ~(lane_free("RIGHT")) & ~tr_violation(VAR, ID, "Turn right")
    Can_turn_right(VAR, ID) <= object_in_lane(VAR, ID, "LEFT") & lane_free("MIDDLE")
    Can_turn_right(VAR, ID) <= object_in_lane(VAR, ID, "LEFT") & ~(lane_free("MIDDLE")) & ~tr_violation(VAR, ID, "Turn right")

def selfcheck():
    Does_varient_selfcontradict(VAR, ID, ACTION) <= suggested_action(VAR, ID, ACTION) & tr_violation(VAR, ID, ACTION)

def crosscheck():
    Does_varients_contradict(V1, V2, ID, ACTION) <= suggested_action(V1, ID, ACTION) & tr_violation(V2, ID, ACTION)



def varient_policy1(varient, idx):
    """
    Actions: turn left, turn right, stop
    Policy:
        if continuing is not possible, try to turn left;
        if not possible, try to turn right;
        if not possible, stop.
    Note:
        for side cars, "stop" means the speed of side cars change to the negative speed of  maincar; other road objects remain the same
        for maincar, "stop" means speed of other road objects is increased by the speed of maincar
    """
    loadrule()
    if Can_turn_left('v1', idx):
        # print(idx, 'can turn left')
        action = action_priority[0]
    elif Can_turn_right('v1', idx):
        # print(idx, 'can turn right')
        action = action_priority[1]
    else:
        # print(idx, 'can only stop')
        action = action_priority[2]
    + suggested_action('v1', idx, action)


def varient_policy2(varient, idx):
    """
    Actions: turn left, turn right, decelerate
    Policy:
        if continuing is not possible, try to turn left;
        if not possible, try to turn right;
        if not possible, decelerate until it stops.
    """
    loadrule()
    if Can_turn_right('v2', idx):
        # print(idx, 'can turn left')
        action = 'Turn right'
    elif Can_turn_left('v2', idx):
        # print(idx, 'can turn right')
        action = 'Turn left'
    else:
        # print(idx, 'reduce speed')
        action = 'Stop'
    + suggested_action('v2', idx, action)


def varient1(idlst):
    for id in idlst:
        varient_policy1('v1', id)
    # print("suggested action1")
    # print(suggested_action(VAR, ID, ACTION))
    asw = (ask('suggested_action(v1, ID_v1, ACTION_v1)'))
    # print("asw")
    dct = dict(asw.answers)
    if dct == None:
        dct = {}
    selfcheck()
    for i in dct:
        if Does_varient_selfcontradict("v1", i, dct[i]):
            print("violation within varient1!")
    return dct


def varient2(idlst):
    for id in idlst:
        varient_policy2('v2', id)
    print("suggested action2")
    print(suggested_action(VAR, ID, ACTION))
    # print(ask('suggested_action(v2, ID_v2, ACTION_v2)'))
    asw = (ask('suggested_action(v2, ID_v2, ACTION_v2)'))
    print("asw")
    dct = dict(asw.answers)
    if dct == None:
        dct = {}
    selfcheck()
    for i in dct:
        if Does_varient_selfcontradict("v2", i, dct[i]):
            print("violation within varient2")
    return dct


def supervisor(roadobjs, dct1, dct2):
    """check contradiction and decide actions"""
    print("dct1", dct1)
    print("dct2", dct2)
    print("violations")
    print(tr_violation(VAR, ID, ACTION))
    if dct1 == [] and dct2 == []:
        dct = supervisor_policy1(dct1)
    elif not dct1:
        dct = supervisor_policy1(dct2)
    elif not dct2:
        dct = supervisor_policy1(dct1)
    else:
        flag1 = False
        flag2 = False
        crosscheck()
        for id in dct1.keys():
            if Does_varients_contradict("v1", "v2", id, dct1[id]):
                flag1 = True
        for id in dct2.keys():
            if Does_varients_contradict("v2", "v1", id, dct2[id]):
                flag2 = True
        print('flag1', flag1, 'flag2', flag2)
        if flag1 and flag2:
            terminateflag = supervisor_policy3()
            return terminateflag
        elif flag1 or flag2:
            dct = supervisor_policy2(flag1, flag2, dct1, dct2)
        else:
            dct = supervisor_policy1(dct1)

    dct = tookaction(dct.keys(), dct, roadobjs)
    terminateflag = supervisor_component(dct, roadobjs)
    return terminateflag


def supervisor_policy1(dct1):
    "take decisions of varient1"
    print("take policy 1")
    return dct1


def supervisor_policy2(flag1, flag2, dct1, dct2):
    "take decisions of varient1"
    print("take policy 2")
    if flag1 == True:
        print("choose decisions of varient2")
        return dct2
    elif flag2 == True:
        print("choose decisions of varient1")
        return dct1


def supervisor_policy3():
    "invoke panic"
    print("take policy 3")
    print("panic")
    return True


def supervisor_component(mydict, roadobjs):
    global DICT_LAST
    print('DICT_LAST',DICT_LAST)
    print('mydict',mydict)
    if DICT_LAST:
        n = NUM_OF_OBJ - 1
        if n in mydict and n in DICT_LAST and DICT_LAST[n] == 'Stop' and mydict[n] == 'Stop':
            global global_count
            global_count += 1
            print('continuously stop', global_count, 'times')
            if global_count > 5:
                return True
            for obj in roadobjs:
                obj.accelerate()
    DICT_LAST = mydict.copy()
    return False


def calculateDistance(l1, l2):
    dist = math.sqrt((l2[0] - l1[0]) ** 2 + (l2[1] - l1[1]) ** 2)
    return dist


def changeAction(dict, id):
    act = dict[id]
    print("original action:", act)
    if act == action_priority[0] and Can_turn_right(id):
        dict[id] = action_priority[1]
        print("change to action:", dict[id])
    elif act == action_priority[1]:
        dict[id] = action_priority[2]
        print("change to action:", dict[id])
    else:
        print("collide!")


def setTransformingSpeed(obj, dict):
    act = dict[obj.idx]
    if act == "Turn left":
        obj.turnleft()
    elif act == "Turn right":
        obj.turnright()
    elif act == 'Stop':
        obj.stop()
    elif act == 'Decelerate':
        obj.xspeed = obj.xspeed - 0.5


def get_key(dict, value):
    return [k for k, v in dict.items() if v == value]


def checkviolation(roadobjs, dict):
    n = NUM_OF_OBJ
    if n - 1 in dict.keys():
        act = dict[n - 1]
        lst = get_key(dict, act)
        lst.remove(n - 1)
        for item in lst:
            if roadobjs[item].getlane() == roadobjs[n - 1].getlane() and \
                    abs(roadobjs[item].getxpos() - roadobjs[n - 1].getxpos()) < COLLIDELIMIT:
                del dict[n - 1]
    pos_matrix = {}
    # pos_matrix2 = {}
    for obj in roadobjs:
        # pos_matrix2[obj.idx] = [obj.getxpos(), obj.getypos()]
        pos_matrix[obj.idx] = [obj.getxpos() + obj.xspeed * 20, obj.getypos() + obj.yspeed * 20]
    # print('before move', pos_matrix2)
    # print('after move', pos_matrix)
    # n = NUM_OF_OBJ
    for id1 in range(0, n - 1):
        for id2 in range(id1 + 1, n):
            dis = calculateDistance(pos_matrix[id1], pos_matrix[id2])
            if dis <= 100:
                print('problem!!!!!', dis, dict, pos_matrix[id1], pos_matrix[id2])
                print(roadobjs[id1].type, roadobjs[id1].idx, roadobjs[id1].getxpos(), roadobjs[id1].getypos())
                print(roadobjs[id2].type, roadobjs[id2].idx, roadobjs[id2].getxpos(), roadobjs[id2].getypos())
                # time.sleep(3)
                if id1 in dict.keys():
                    roadobjs[id1].yspeed = 0
                    dict[id1] = action_priority[2]
                    setTransformingSpeed(roadobjs[id1], dict)
                if id2 in dict.keys():
                    roadobjs[id2].yspeed = 0
                    dict[id2] = action_priority[2]
                    setTransformingSpeed(roadobjs[id2], dict)
    return dict


def tookaction(action_id, dict, roadobjs):
    if action_id:
        for obj in roadobjs:
            if obj.idx in action_id:
                setTransformingSpeed(obj, dict)
    dict = checkviolation(roadobjs, dict)
    return dict
