from tkinter_component import *
import random

TURN_STEP = 7
LANEGAP = 140
TURN_LOOP = LANEGAP/TURN_STEP
MAINCAR_SPEED = 3
SIDECAR_SPEED = [3]
LEFT_DIRECTION = -1
RIGHT_DIRECTION = 1
LANE = [LEFT, MIDDLE, RIGHT]


class Tree:
    def __init__(self, xpos, ypos):
        self.image = tk.PhotoImage(file="tree.png")
        self.shape = canvas.create_image(xpos, ypos, image=self.image, anchor=tk.NW)
        self.xspeed = -MAINCAR_SPEED

    def move(self):
        canvas.move(self.shape, -1, 0)
        pos = canvas.coords(self.shape)
        # print(self.pos)
        if pos[0] < 0:
            canvas.move(self.shape, WIDTH, 0)


class RoadObject:
    """
    define main car and obstacles, including side cars and roadblocks
    speed:
        for maincar, speed relative to camera is 0
        for sidecar, speed is randomly chosen
        for roadblock, speed is negative speed of maincar
    """
    def __init__(self, xpos, ypos, type, idx):
        if type == "maincar":
            self.image = tk.PhotoImage(file="Car_main.png")
            self.xspeed = 0
        elif type == "sidecar":
            self.image = tk.PhotoImage(file="Car_side.png")
            self.xspeed = random.choice(SIDECAR_SPEED)
        elif type == "roadblock":
            self.image = tk.PhotoImage(file="roadblock.png")
            self.xspeed = -MAINCAR_SPEED
        self.idx = idx
        self.state = "initial"
        self.inispeed = self.xspeed
        self.type = type
        self.shape = canvas.create_image(xpos, ypos, image=self.image, anchor=tk.NW)
        self.turningstep_count = 0 # count the steps of "turning" movement
        self.yspeed = 0

    def getxpos(self):
        xpos = int(canvas.coords(self.shape)[0])
        return xpos

    def getypos(self):
        ypos = int(canvas.coords(self.shape)[1])
        return ypos

    def getlane(self):
        if self.getypos() == LEFT:
            return "LEFT"
        elif self.getypos() == MIDDLE:
            return "MIDDLE"
        elif self.getypos() == RIGHT:
            return "RIGHT"
        else:
            return None

    def outofcanvas(self):
        pos = canvas.coords(self.shape)
        nypos = random.choice(LANE)
        if self.type == "sidecar" and pos[0] > WIDTH:
            canvas.move(self.shape, -WIDTH - 130, 0)
        elif self.type == "roadblock" and pos[0] < -120:
            canvas.move(self.shape, WIDTH + 130, -pos[1] + nypos)

    def turnleft(self):
        self.yspeed = -TURN_STEP

    def turnright(self):
        self.yspeed = TURN_STEP

    def stop(self):
        self.xspeed = -MAINCAR_SPEED
        self.state = "stopped"

    def accelerate(self):
        if self.type != "maincar":
            self.xspeed += MAINCAR_SPEED
        else:
            self.xspeed = 0

    def reverse(self):
        self.yspeed = 0
        self.xspeed = self.inispeed
        if self.state == "stopped" and self.type == "sidecar":
            self.state = "initial"
        elif self.state == "reverse" and self.type == "maincar":
            self.xspeed = 0
            self.state = "initial"
            print("main car become initial")
        elif self.state == "stopped" and self.type == "maincar":
            self.xspeed = MAINCAR_SPEED
            self.state = "reverse"
            print("main car reverse")


