import tkinter as tk


WIDTH = 600
HEIGHT = 420
GREENBELT = 28
LEFT = HEIGHT / 6 - 60 + GREENBELT
MIDDLE = HEIGHT / 2 - 60 + GREENBELT
RIGHT = HEIGHT / 6 * 5 - 60 + GREENBELT
PICWIDTH = 120
UPDATE_INTERVAL = 0.02

window = tk.Tk()
window.title("Animation")
canvas = tk.Canvas(window, width=WIDTH, height=HEIGHT + GREENBELT*2, bg='#5B5F5E')
canvas.pack()
canvas.create_rectangle((0, 0, WIDTH, GREENBELT), fill="#DDDDDD", outline="")
canvas.create_rectangle((0, HEIGHT+GREENBELT, WIDTH, HEIGHT+GREENBELT*2), fill="#DDDDDD", outline="")
canvas.create_rectangle((0, HEIGHT/3 - 2.5 + GREENBELT, WIDTH, HEIGHT/3 + 2.5 +GREENBELT), fill="white", outline="")
canvas.create_rectangle((0, HEIGHT/3*2 - 2.5 + GREENBELT, WIDTH, HEIGHT/3*2 + 2.5 + GREENBELT), fill="white", outline="")

# var = tk.StringVar()
# lab1=tk.Label(window, textvariable=var)
# lab1.place(x=0, y=0, width=40, height=20, anchor=tk.NW)

